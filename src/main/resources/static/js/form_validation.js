//Взято с https://bootstrap-4.ru/docs/5.0/forms/validation/
function validateForm() {
    'use strict'

    const forms = document.querySelectorAll('.needs-validation');


    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }
                form.classList.add('was-validated')
            }, false)
        })
}