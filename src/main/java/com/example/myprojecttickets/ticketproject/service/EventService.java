package com.example.myprojecttickets.ticketproject.service;

import com.example.myprojecttickets.ticketproject.constants.Errors;
import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.exception.MyDeleteException;
import com.example.myprojecttickets.ticketproject.mapper.EventMapper;
import com.example.myprojecttickets.ticketproject.model.Event;
import com.example.myprojecttickets.ticketproject.repository.EventRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
@Slf4j

public class EventService
      extends GenericService<Event, EventDTO> {
    
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository,
                        EventMapper eventMapper) {
        super(eventRepository, eventMapper);
        this.eventRepository = eventRepository;

    }

    public Page<EventDTO> searchEvent(final String name,
                                      Pageable pageable) {
        Page<Event> events = eventRepository.findAllByNameEventContainsIgnoreCaseAndIsDeletedFalse(name, pageable);
        List<EventDTO> result = mapper.toDTOs(events.getContent());
        return new PageImpl<>(result, pageable, events.getTotalElements());
    }

    @Override
    public void delete(Long id) throws MyDeleteException {
        Event event = eventRepository.findById(id).orElseThrow(
              () -> new NotFoundException("События с заданным id=" + id + " не существует."));

        boolean eventCanBeDeleted = eventRepository.checkEventForDeletion(id);
        if (eventCanBeDeleted) {
            markAsDeleted(event);
            repository.save(event);
        }
        else {
            throw new MyDeleteException(Errors.Event.EVENT_DELETE_ERROR);
        }

    }

    public void restore(Long objectId) {
        Event event = eventRepository.findById(objectId).orElseThrow(
              () -> new NotFoundException("События с заданным id=" + objectId + " не существует."));
        unMarkAsDeleted(event);
        eventRepository.save(event);
    }
}
