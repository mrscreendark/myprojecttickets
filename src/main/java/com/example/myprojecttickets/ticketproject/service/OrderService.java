package com.example.myprojecttickets.ticketproject.service;


import com.example.myprojecttickets.ticketproject.constants.MailConstants;
import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.dto.OrderDTO;
import com.example.myprojecttickets.ticketproject.dto.UserDTO;
import com.example.myprojecttickets.ticketproject.exception.MyDeleteException;
import com.example.myprojecttickets.ticketproject.mapper.OrderMapper;
import com.example.myprojecttickets.ticketproject.model.Order;
import com.example.myprojecttickets.ticketproject.model.Status;
import com.example.myprojecttickets.ticketproject.repository.OrderRepository;
import com.example.myprojecttickets.ticketproject.utils.FileHelper;
import com.example.myprojecttickets.ticketproject.utils.MailUtils;
import groovy.util.logging.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class OrderService
      extends GenericService<Order, OrderDTO> {

    private final JavaMailSender javaMailSender;
    private final EventService eventService;
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final UserService userService;



    protected OrderService(
            JavaMailSender javaMailSender, OrderMapper orderMapper,
            EventService eventService, OrderRepository orderRepository, UserService userService) {
        super(orderRepository, orderMapper);
        this.javaMailSender = javaMailSender;
        this.eventService = eventService;
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.userService = userService;
    }
    public Page<OrderDTO> listUserOrderTickets(final Long id,
                                                   final Pageable pageable) {
        Page<Order> objects = orderRepository.getOrderTicketsByUserId(id, pageable);
        List<OrderDTO> results = orderMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());

    }
        public OrderDTO buyTicket(OrderDTO orderTicketDTO) throws IOException {
        EventDTO eventDTO = eventService.getOne(orderTicketDTO.getEventId());
        LocalDate date = LocalDate.parse(eventDTO.getDateRealize());
        LocalDate date2 = LocalDate.now();
        UserDTO userDTO = userService.getOne(orderTicketDTO.getUserId());

        if(date.isAfter(date2) && eventDTO.getAmountTickets() > 0){
            int random = (int)(Math.random() * 1000000);

            String fileName = FileHelper.createFile(orderTicketDTO, eventService, random);
            orderTicketDTO.setOnlineCopyPath(fileName);

            eventDTO.setAmountTickets(eventDTO.getAmountTickets() - 1);
            orderTicketDTO.setBoughtDate(LocalDateTime.now());
            orderTicketDTO.setStatus(Status.BOUGHT);
            eventService.update(eventDTO);

            System.out.println("Привет из сервиса ордер" + orderTicketDTO.toString());

//            SimpleMailMessage mailMessage = MailUtils.createEmailMessage(userDTO.getEmail(),
//                    MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD,
//                    MailConstants.MAIL_MESSAGE_FOR_BUY_TICKET);
//            javaMailSender.send(mailMessage);
        }
        return mapper.toDTO(repository.save(mapper.toEntity(orderTicketDTO)));
    }

    public void buyTicketFinal(Long id) throws IOException {
        OrderDTO orderDTO = getOne(id);
        EventDTO eventDTO = eventService.getOne(orderDTO.getEventId());
        UserDTO userDTO = userService.getOne(orderDTO.getUserId());

        LocalDate date = LocalDate.parse(eventDTO.getDateRealize());
        LocalDate date2 = LocalDate.now();

        if(date.isAfter(date2) && eventDTO.getAmountTickets() > 0 && !eventDTO.isDeleted()){
            int random = (int)(Math.random() * 1000000);

            String fileName = FileHelper.createFile(orderDTO, eventService, random);
            orderDTO.setOnlineCopyPath(fileName);

            orderDTO.setBoughtDate(LocalDateTime.now());
            orderDTO.setBookingDate(null);
            orderDTO.setStatus(Status.BOUGHT);
            eventService.update(eventDTO);
            update(orderDTO);

//            SimpleMailMessage mailMessage = MailUtils.createEmailMessage(userDTO.getEmail(),
//                    MailConstants.MAIL_SUBJECT_FOR_BUY,
//                    MailConstants.MAIL_MESSAGE_FOR_BUY_TICKET);
//            javaMailSender.send(mailMessage);
        }

    }

    public OrderDTO bookingTicket(OrderDTO orderTicketDTO) {
        EventDTO eventDTO = eventService.getOne(orderTicketDTO.getEventId());
        UserDTO userDTO = userService.getOne(orderTicketDTO.getUserId());

        LocalDate date = LocalDate.parse(eventDTO.getDateReserveStop());
        LocalDate date2 = LocalDate.now();

        if(date.isAfter(date2) && eventDTO.getAmountTickets()>0){
            eventDTO.setAmountTickets(eventDTO.getAmountTickets() - 1);
            eventService.update(eventDTO);
            orderTicketDTO.setBookingDate(LocalDateTime.now());
            orderTicketDTO.setStatus(Status.RESERVE);

//            SimpleMailMessage mailMessage = MailUtils.createEmailMessage(userDTO.getEmail(),
//                    MailConstants.MAIL_SUBJECT_FOR_BOOKING,
//                    MailConstants.MAIL_MESSAGE_FOR_BOOKING_TICKET);
//            javaMailSender.send(mailMessage);
        }
        return mapper.toDTO(repository.save(mapper.toEntity(orderTicketDTO)));
    }

    public void returnTicket(final Long id) throws FileNotFoundException {
        Order order = repository.findById(id).orElseThrow(
                () -> new NotFoundException("Заказа " + id + " не существует"));
        OrderDTO orderDTO = getOne(id);
        EventDTO eventDTO = eventService.getOne(orderDTO.getEventId());
        try{
        if (orderDTO.getOnlineCopyPath() != null && !orderDTO.getOnlineCopyPath().isEmpty()) {
            FileHelper.deleteFile(orderDTO.getOnlineCopyPath());
        }} catch (Exception e) {

            throw new FileNotFoundException();

        } finally {
            eventDTO.setAmountTickets(eventDTO.getAmountTickets() + 1);
            eventService.update(eventDTO);
            orderRepository.delete(order);
        }
    }
}
