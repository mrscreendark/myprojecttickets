package com.example.myprojecttickets.ticketproject.MVCcontroller;


import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.exception.MyDeleteException;
import com.example.myprojecttickets.ticketproject.service.EventService;
import com.example.myprojecttickets.ticketproject.service.OrderService;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import static com.example.myprojecttickets.ticketproject.constants.UserRolesConstants.ADMIN;

@Controller
@Hidden
@RequestMapping("/events")
@Slf4j
public class MVCEventController {
    
    private final EventService eventService;

    private final OrderService orderService;

    public MVCEventController(EventService eventService
                              ,OrderService orderService) {
        this.eventService = eventService;
        this.orderService = orderService;
    }
    
    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         @ModelAttribute(name = "exception") final String exception,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "nameEvent"));
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        Page<EventDTO> result;
        if (ADMIN.equalsIgnoreCase(userName)) {
            result = eventService.listAll(pageRequest);
        }
        else {
            result = eventService.listAllNotDeleted(pageRequest);
        }
        model.addAttribute("events", result);
        model.addAttribute("exception", exception);
        return "events/viewAllEvents";
    }
    
    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("event", eventService.getOne(id));
        return "events/viewEvent";
    }
    
    @GetMapping("/add")
    public String create() {
        return "events/addEvent";
    }
    
    @PostMapping("/add")
    public String create(@ModelAttribute("eventForm") EventDTO eventDTO) {
        eventService.create(eventDTO);
        return "redirect:/events";
    }
    
    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("event", eventService.getOne(id));
        return "events/updateEvent";
    }
    
    @PostMapping("/update")
    public String update(@ModelAttribute("eventForm") EventDTO eventDTO) {
        log.info("Привет из контрллера" + eventDTO.toString());
        eventService.update(eventDTO);
        return "redirect:/events";
    }

    @PostMapping("/search")
    public String searchEvent(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("eventSearchForm") EventDTO eventDTO,
                              Model model) {
        if (StringUtils.hasText(eventDTO.getNameEvent()) || StringUtils.hasLength(eventDTO.getNameEvent())) {
            PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "nameEvent"));
            model.addAttribute("events", eventService.searchEvent(eventDTO.getNameEvent().trim(), pageRequest));
            return "events/viewAllEvents";
        }
        else {
            return "redirect:/events";
        }
    }
    
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        eventService.delete(id);
        return "redirect:/events";
    }
    
    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        eventService.restore(id);
        return "redirect:/events";
    }
    
    @ExceptionHandler(MyDeleteException.class)
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/events", true);
    }
}
