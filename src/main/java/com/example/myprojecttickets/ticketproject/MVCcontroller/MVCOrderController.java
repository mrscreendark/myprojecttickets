package com.example.myprojecttickets.ticketproject.MVCcontroller;



import com.example.myprojecttickets.ticketproject.dto.OrderDTO;
import com.example.myprojecttickets.ticketproject.exception.MyDeleteException;
import com.example.myprojecttickets.ticketproject.service.EventService;
import com.example.myprojecttickets.ticketproject.service.OrderService;
import com.example.myprojecttickets.ticketproject.service.userdetails.CustomUserDetails;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Hidden
@Controller
@RequestMapping("/order")
@Slf4j
public class MVCOrderController {
    private final OrderService orderService;
    private final EventService eventService;

    public MVCOrderController(OrderService orderService, EventService eventService) {
        this.orderService = orderService;
        this.eventService = eventService;
    }

    @GetMapping("/ticketbuy/{eventId}")
    public String buyTicket(@PathVariable Long eventId,
                           Model model) {
        model.addAttribute("event", eventService.getOne(eventId));
        return "userTickets/buyTicket";
    }

    @PostMapping("/ticketbuy")
    public String buyTicket(@ModelAttribute("orderTicketForm") OrderDTO orderDTO) throws IOException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        orderDTO.setUserId(Long.valueOf(customUserDetails.getUserId()));
        orderService.buyTicket(orderDTO);
        return "redirect:/order/user-tickets/" + customUserDetails.getUserId();
    }

    @GetMapping("/ticketbooking/{eventId}")
    public String bookingTicket(@PathVariable Long eventId,
                            Model model) {
        model.addAttribute("event", eventService.getOne(eventId));
        return "userTickets/bookingTicket";
    }

    @PostMapping("/ticketbooking")
    public String bookingTicket(@ModelAttribute("orderTicketForm") OrderDTO orderDTO) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        orderDTO.setUserId(Long.valueOf(customUserDetails.getUserId()));
        orderService.bookingTicket(orderDTO);
        return "redirect:/order/user-tickets/" + customUserDetails.getUserId();
    }

    @GetMapping("/ticketbuyfinal/{id}")
    public String buyTicketFinal(@PathVariable Long id) throws MyDeleteException, IOException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        orderService.buyTicketFinal(id);
        return "redirect:/order/user-tickets/" + customUserDetails.getUserId();
    }

    @GetMapping("/return-ticket/{id}")
    public String returnTicket(@PathVariable Long id) throws MyDeleteException, FileNotFoundException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        orderService.returnTicket(id);
        return "redirect:/order/user-tickets/" + customUserDetails.getUserId();
    }
    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadBook(@Param(value = "orderId") Long orderId) throws IOException {
        OrderDTO orderDTO = orderService.getOne(orderId);
        Path path = Paths.get(orderDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }
    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }

    @GetMapping("/user-tickets/{id}")
    public String userTickets(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "10") int pageSize,
                            @PathVariable Long id,
                            Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Page<OrderDTO> rentInfoDTOPage = orderService.listUserOrderTickets(id, pageRequest);
        model.addAttribute("orderTickets", rentInfoDTOPage);
        return "userTickets/viewAllUserTicket";
    }




}
