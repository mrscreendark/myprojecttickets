package com.example.myprojecttickets.ticketproject.config;


import com.example.myprojecttickets.ticketproject.exception.MyDeleteException;
import com.example.myprojecttickets.ticketproject.service.OrderService;
import com.example.myprojecttickets.ticketproject.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.util.List;

@Component
@Slf4j
public class MailScheduler {
    private UserService userService;
    private OrderService orderService;
    private JavaMailSender javaMailSender;
    
    @Autowired
    public void getUserService(UserService userService) {
        this.userService = userService;
    }
    @Autowired
    public void getEmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
    @Autowired
    public void getOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @Scheduled(cron = "0 0 6 * * ?") //каждый день в 6 утра
    public void sendMailsEndOfBooking() throws MyDeleteException, FileNotFoundException {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        List<String> emails = userService.getUserEmailsWithEndOfBooking();
        if (emails.size() > 0) {
            mailMessage.setTo(emails.toArray(new String[0]));
            mailMessage.setSubject("Приветствие");
            mailMessage.setText("Добрый день! Вы получили это письмо, так как время бронирования билета подошло к концу!\n  " +
                    "Ваша бронь была снята");
            javaMailSender.send(mailMessage);
        }
       orderService.returnTicket(userService.getUsersOrderEmailsWithEndOfBooking());
        log.info("Чистельщик брони");
    }
}
