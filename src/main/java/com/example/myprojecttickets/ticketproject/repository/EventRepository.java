package com.example.myprojecttickets.ticketproject.repository;

import com.example.myprojecttickets.ticketproject.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


//Ввести сюда логигу проверки на конеч6ность билетов

@Repository
public interface EventRepository
      extends GenericRepository<Event> {
    
    Page<Event> findAllByIsDeletedFalse(Pageable pageable);
    
    Page<Event> findAllByNameEventContainsIgnoreCaseAndIsDeletedFalse(String fio,
                                                                       Pageable pageable);
    @Query("""
          select case when count(e) > 0  then false else true end
          from Event e join Order o on e.id = o.event.id
          where e.id = :eventId and o.status = 0 and o.status = 1
          """)
    boolean checkEventForDeletion(final Long eventId);

}
