package com.example.myprojecttickets.ticketproject.repository;


import com.example.myprojecttickets.ticketproject.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository
      extends GenericRepository<User> {
    
    //select * from users where login = ?
//    @Query(nativeQuery = true, value = "select * from users where login = :login")
    User findUserByLogin(String login);
    
    User findUserByLoginAndIsDeletedFalse(String login);
    
    User findUserByEmail(String email);
    
    User findUserByChangePasswordToken(String token);
    
    @Query(nativeQuery = true,
           value = """
                 select u.*
                 from users u
                 where u.first_name ilike '%' || coalesce(:firstName, '%') || '%'
                 and u.last_name ilike '%' || coalesce(:lastName, '%') || '%'
                 and u.login ilike '%' || coalesce(:login, '%') || '%'
                  """)
    Page<User> searchUsers(String firstName,
                           String lastName,
                           String login,
                           Pageable pageable);
    
    @Query(nativeQuery = true,
           value = """
                 select email
                 from events e join orders o on e.id = o.event_id,
                 o join users u on u.id = o.user_id
                 where e.date_reserve_stop >= now()
                 and o.status = 0
                 and u.is_deleted = false
                 and e.is_deleted = false
                 """)
    List<String> endOfBooking();

    @Query(nativeQuery = true,
            value = """
                 select o.id
                 from events e join orders o on e.id = o.event_id,
                 o join users u on u.id = o.user_id
                 where e.date_reserve_stop >= now()
                 and o.status = 0
                 and u.is_deleted = false
                 and e.is_deleted = false
                 """)
    Long deleteEndOfBooking();
}
