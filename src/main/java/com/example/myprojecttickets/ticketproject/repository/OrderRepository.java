package com.example.myprojecttickets.ticketproject.repository;

import com.example.myprojecttickets.ticketproject.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository
      extends GenericRepository<Order> {
    Page<Order> getOrderTicketsByUserId(Long userId,
                                               Pageable pageable);


}
