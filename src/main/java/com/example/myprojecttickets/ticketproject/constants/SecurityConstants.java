package com.example.myprojecttickets.ticketproject.constants;

import java.util.List;

public interface SecurityConstants {


    public static List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
                                                "/js/**",
                                                "/css/**",
                                                "/",
                                                "/swagger-ui/**",
                                                "/webjars/bootstrap/5.0.2/**",
                                                "/v3/api-docs/**",
                                                "/error");

    public static List<String> EVENTS_WHITE_LIST = List.of("/events",
                                              "/events/search",
                                              "/tickets/search/event",
                                              "/events/{id}");

    public static List<String> EVENTS_PERMISSION_LIST = List.of("/events/add",
                                                   "/events/update",
                                                   "/events/delete");

    public static List<String> USERS_WHITE_LIST = List.of("/login",
                                            "/users/registration",
                                            "/users/remember-password",
                                            "/users/change-password");

    public static List<String> USERS_PERMISSION_LIST = List.of("/order/ticketbuy/*",
                                                 "/order/ticketbooking/*");
}
