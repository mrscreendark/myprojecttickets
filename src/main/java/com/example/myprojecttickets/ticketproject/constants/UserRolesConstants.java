package com.example.myprojecttickets.ticketproject.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String USHER = "USHER";
    String USER = "USER";
}
