package com.example.myprojecttickets.ticketproject.constants;

public interface Errors {

    
    class Event{
        public static final String EVENT_DELETE_ERROR = "Событие не может быть удалено, так как у него уже есть купленные билеты";
    }
    
    class Users{
        public static final String USER_FORBIDDEN_ERROR = "У вас нет прав просматривать информацию о пользователе";
    }
}
