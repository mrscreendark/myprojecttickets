package com.example.myprojecttickets.ticketproject.constants;

public interface MailConstants {
    String MAIL_MESSAGE_FOR_REMEMBER_PASSWORD = """
                    Добрый день. Вы получили это письмо, так как с вашего аккаунта была отправлена заявка на восстановление пароля.\n
                    Для восстановления пароля перейдите по ссылке: http://localhost:9070/users/change-password?uuid=""";
    
    String MAIL_SUBJECT_FOR_REMEMBER_PASSWORD = "Восстановление пароля на сайте Продажи билетов";

    String MAIL_MESSAGE_FOR_BUY_TICKET = "Добрый день. Поздравляем вас с покупкой билета на нашем сервисе!\n" +
            "Скачать билет и ознакомится с информацией о событии вы может на нашем сервисе";

    String MAIL_MESSAGE_FOR_BOOKING_TICKET = "Добрый день. Вы забронировали билет на нашем сервисе на нашем сервисе! \n" +
            "Выкупить билет и ознакомится с информацией вы можете на нашем сервисе";

    String MAIL_SUBJECT_FOR_BUY = "Покупка билета. ВебКасса";
    String MAIL_SUBJECT_FOR_BOOKING = "бронирование билета. ВебКасса";

}
