package com.example.myprojecttickets.ticketproject.mapper;

import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.model.Event;
import com.example.myprojecttickets.ticketproject.model.GenericModel;
import com.example.myprojecttickets.ticketproject.repository.OrderRepository;
import com.example.myprojecttickets.ticketproject.utils.DateFormatter;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class EventMapper
      extends GenericMapper<Event, EventDTO> {



    public EventMapper(ModelMapper mapper) {
        super(mapper, Event.class, EventDTO.class);

    }


    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(EventDTO.class, Event.class)
              .addMappings(m -> m.skip(Event::setDateRealize)).setPostConverter(toEntityConverter())
              .addMappings(m -> m.skip(Event::setDateReserveStop)).setPostConverter(toEntityConverter());
    }
    
    @Override
    protected void mapSpecificFields(EventDTO source, Event destination) {
        destination.setDateRealize(DateFormatter.formatStringToDate(source.getDateRealize()));
        destination.setDateReserveStop(DateFormatter.formatStringToDate(source.getDateRealize()).minusDays(1));


    }
    
    @Override
    protected void mapSpecificFields(Event source, EventDTO destination) {
    }

    @Override
    protected Set<Long> getIds(Event entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

}
