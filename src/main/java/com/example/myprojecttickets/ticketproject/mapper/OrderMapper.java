package com.example.myprojecttickets.ticketproject.mapper;

import com.example.myprojecttickets.ticketproject.dto.OrderDTO;
import com.example.myprojecttickets.ticketproject.model.Order;
import com.example.myprojecttickets.ticketproject.repository.EventRepository;
import com.example.myprojecttickets.ticketproject.repository.UserRepository;
import com.example.myprojecttickets.ticketproject.service.EventService;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;

@Component
public class OrderMapper
      extends GenericMapper<Order, OrderDTO> {

    private final UserRepository userRepository;
    private final EventRepository eventRepository;
    private final EventService eventService;

    
    protected OrderMapper(ModelMapper mapper,
                          UserRepository userRepository, EventRepository eventRepository, EventService eventService) {
        super(mapper, Order.class, OrderDTO.class);
        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
        this.eventService = eventService;
    }
    
    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
              .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDtoConverter())
              .addMappings(m -> m.skip(OrderDTO::setEventId)).setPostConverter(toDtoConverter())
              .addMappings(m -> m.skip(OrderDTO::setEventDTO)).setPostConverter(toDtoConverter());
        
        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
              .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
              .addMappings(m -> m.skip(Order::setEvent)).setPostConverter(toEntityConverter());
    }
    
    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setEvent(eventRepository.findById(source.getEventId()).orElseThrow(() -> new NotFoundException("Билета не найдено")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }
    
    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setEventId(source.getEvent().getId());
        destination.setEventDTO(eventService.getOne(source.getEvent().getId()));
    }
    
    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
