package com.example.myprojecttickets.ticketproject.utils;

import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.dto.OrderDTO;
import com.example.myprojecttickets.ticketproject.service.EventService;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.example.myprojecttickets.ticketproject.constants.FileDirectoriesConstants.TICKETS_UPLOAD_DIRECTORY;

@Slf4j
public class FileHelper {

    
    public static String createFile(OrderDTO orderDTO, EventService eventService, int random) throws IOException {
        EventDTO eventDTO = eventService.getOne(orderDTO.getEventId());

        String data = "Код билета - " + random +
                "\n" + "Название события - " + eventDTO.getNameEvent() +
                "\n" + "Дата проведения - " + eventDTO.getDateRealize() +
                "\n" + "Место проведения - " + eventDTO.getPlace() +
                "\n" + "Цена билета - " +  eventDTO.getPrice();
        String resultFileName = "Ticket number" + random;

        Path path = Paths.get(TICKETS_UPLOAD_DIRECTORY + "\\" + resultFileName + ".txt");
        try {
            Path createdFilePath = Files.createFile(path);
            System.out.println("File created at path: " + createdFilePath);
            Files.write(path, data.getBytes());
        }
        catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
        }

        return TICKETS_UPLOAD_DIRECTORY + "\\" + resultFileName + ".txt";
    }
    
    public static void deleteFile(String filePath){
        Path path = Paths.get(filePath);

        try {
            Files.deleteIfExists(path);
        }
        catch (IOException ex) {
            log.error("FileHelper#deleteFile(): {}", ex.getMessage());
        }
    }
}
