package com.example.myprojecttickets.ticketproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "order_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Order
      extends GenericModel {

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "event_id", foreignKey = @ForeignKey(name = "FK_ORDER_EVENT"))
    private Event event;
    
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_ORDER_USER"))
    private User user;

    @Column(name = "online_copy_path")
    private String onlineCopyPath;
    
    @Column(name = "booking_date", nullable = true)
    private LocalDateTime bookingDate;
    //Бронирование снимается за день до мероприятия

    @Column(name = "bought_date", nullable = true)
    private LocalDateTime boughtDate;

    @Column(name = "status", nullable = true)
    @Enumerated
    private Status status;
    //По умолчанию статус "Свободный", однако меняется после действий пользователя



}
