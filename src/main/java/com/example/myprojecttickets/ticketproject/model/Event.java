package com.example.myprojecttickets.ticketproject.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "events")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "events_seq", allocationSize = 1)
public class Event
      extends GenericModel {
    @Column(name = "name_event", nullable = false)
    private String nameEvent;

    @Column(name = "place", nullable = false)
    private String place;
    
    @Column(name = "date_realize", nullable = false)
    private LocalDate dateRealize;

    @Column(name = "date_reserve_stop", nullable = true)
    private LocalDate dateReserveStop;
    
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "amount_tickets")
    private Integer amountTickets;

    @Column(name = "price")
    private Integer price;

    @OneToMany(mappedBy = "event", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Order> order;


}
