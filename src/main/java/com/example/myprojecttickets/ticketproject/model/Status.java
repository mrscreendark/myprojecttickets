package com.example.myprojecttickets.ticketproject.model;

public enum Status {
    RESERVE("Забронирован"),
    BOUGHT("Куплен"),
    FREE("Свободен");

    
    private final String statusTextDisplay;

    Status(String text) {
        this.statusTextDisplay = text;
    }
    
    public String getStatusTextDisplay() {
        return this.statusTextDisplay;
    }
}
