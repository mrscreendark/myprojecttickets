package com.example.myprojecttickets.ticketproject.dto;

import com.example.myprojecttickets.ticketproject.model.Status;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderDTO
      extends GenericDTO {

    private LocalDateTime bookingDate;
    private LocalDateTime boughtDate;
    private Status status;
    private String onlineCopyPath;
    private Long eventId;
    private Long userId;
    private EventDTO eventDTO;

}
