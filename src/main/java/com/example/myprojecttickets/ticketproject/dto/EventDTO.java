package com.example.myprojecttickets.ticketproject.dto;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EventDTO
      extends GenericDTO {
    private String nameEvent;
    private String place;
    private String dateRealize;
    private String dateReserveStop;
    private String description;
    private Integer amountTickets;
    private Integer price;
    private boolean isDeleted;
}
