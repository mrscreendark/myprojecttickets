package com.example.myprojecttickets;



import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.model.Event;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface EventTestData {
    EventDTO EVENT_DTO_1 = new EventDTO("name Event",
            "place",
            "dateRealize",
            "dateReserveStop",
            "description",
            99,
            100,
            false);


    EventDTO EVENT_DTO_2 = new EventDTO("name Event",
            "place",
            "dateRealize",
            "dateReserveStop",
            "description",
            100,
            101,
            false);

    EventDTO EVENT_DTO_3_DELETED = new EventDTO("name Event",
            "place",
            "dateRealize",
            "dateReserveStop",
            "description",
            101,
            102,
            false);
    
    List<EventDTO> EVENT_DTO_LIST = Arrays.asList(EVENT_DTO_1, EVENT_DTO_2, EVENT_DTO_3_DELETED);


    Event EVENT_1 = new Event("nameEvent",
            "place",
            LocalDate.now(),
            LocalDate.now(),
            "description",
            99,
            100,
            new HashSet<>());

    Event EVENT_2 = new Event("nameEvent",
            "place",
            LocalDate.now(),
            LocalDate.now(),
            "description",
            100,
            101,
            new HashSet<>());

    Event EVENT_3 = new Event("nameEvent",
            "place",
            LocalDate.now(),
            LocalDate.now(),
            "description",
            101,
            102,
            new HashSet<>());
    
    List<Event> EVENT_LIST = Arrays.asList(EVENT_1, EVENT_2, EVENT_3);
}
