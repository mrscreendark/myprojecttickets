package com.example.myprojecttickets.ticketproject.MVCcontroller;


import com.example.myprojecttickets.EventTestData;
import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.exception.MyDeleteException;
import com.example.myprojecttickets.ticketproject.mapper.EventMapper;
import com.example.myprojecttickets.ticketproject.model.Event;
import com.example.myprojecttickets.ticketproject.repository.EventRepository;
import com.example.myprojecttickets.ticketproject.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class EventServiceTest extends GenericTest<Event, EventDTO> {

    public EventServiceTest() {
        super();
        EventService eventService = Mockito.mock(EventService.class);
        repository = Mockito.mock(EventRepository.class);
        mapper = Mockito.mock(EventMapper.class);
        service = new EventService((EventRepository) repository, (EventMapper) mapper);
    }

    @Test
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(EventTestData.EVENT_LIST);
        Mockito.when(mapper.toDTOs(EventTestData.EVENT_LIST)).thenReturn(EventTestData.EVENT_DTO_LIST);
        List<EventDTO> eventDTOS = service.listAll();
        log.info("Testing getAll(): " + eventDTOS);
        assertEquals(EventTestData.EVENT_LIST.size(), eventDTOS.size());
    }
    
    @Test
    @Override
    protected void getOne() {
//        Mockito.when(repository.findById(1L)).thenReturn(EventTestData.EVENT_LIST);
//        Mockito.when(mapper.toDTOs(EventTestData.EVENT_LIST)).thenReturn(EventTestData.EVENT_DTO_LIST);
//        EventDTO eventDTOS = service.getOne(1L);
//        log.info("Testing getAll(): " + eventDTOS);
//        assertEquals(EventTestData.EVENT_LIST.size(), eventDTOS.size());
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Test
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
