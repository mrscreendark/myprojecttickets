package com.example.myprojecttickets;


import com.example.myprojecttickets.ticketproject.dto.EventDTO;
import com.example.myprojecttickets.ticketproject.dto.OrderDTO;
import com.example.myprojecttickets.ticketproject.model.Event;
import com.example.myprojecttickets.ticketproject.model.Order;
import com.example.myprojecttickets.ticketproject.model.Status;
import com.example.myprojecttickets.ticketproject.model.User;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderTestData {
    
    OrderDTO ORDER_INFO_DTO = new OrderDTO(LocalDateTime.now(),
                                                             LocalDateTime.now(),

                                                             null,
                                                             "onlineCopyPath",
                                                             1L,
                                                             1L,
                                                             new EventDTO());
    
    List<OrderDTO> ORDER_DTO_LIST = List.of(ORDER_INFO_DTO);
    
    Order ORDER_INFO = new Order(new Event(),
            new User(),
            "onlineCopyPath",
            LocalDateTime.now(),
            LocalDateTime.now(),
            null);
    
    List<Order> BOOK_RENT_INFO_LIST = List.of(ORDER_INFO);
}
