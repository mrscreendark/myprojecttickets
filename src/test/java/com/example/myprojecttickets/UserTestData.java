package com.example.myprojecttickets;


import com.example.myprojecttickets.ticketproject.dto.RoleDTO;
import com.example.myprojecttickets.ticketproject.dto.UserDTO;
import com.example.myprojecttickets.ticketproject.model.Role;
import com.example.myprojecttickets.ticketproject.model.User;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

public interface UserTestData {
    
    UserDTO USER_DTO = new UserDTO(
          "login",
          "password",
          "email",
          "birthDate",
          "firstName",
          "lastName",
          "middleName",
          "phone",
          new RoleDTO(),
          "changePasswordToken",
          new HashSet<>(),
          false
    );
    
    List<UserDTO> USER_DTO_LIST = List.of(USER_DTO);
    
    User USER = new User(
          "login",
          "password",
          "email",
          LocalDate.now(),
          "firstName",
          "lastName",
          "middleName",
          "phone",
          "changePasswordToken",
          new Role(),
          new HashSet<>()
    );
    
    List<User> USER_LIST = List.of(USER);
}
